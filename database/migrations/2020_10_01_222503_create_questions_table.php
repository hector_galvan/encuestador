<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->id();
            //crea una llave foranea
            $table->unsignedBigInteger('polls_id')->nullable();
            $table->enum('type', ['open', 'multiple','multiple_answers'])->nulllable();
            $table->longText('question')->nulllable();
            $table->longText('answers')->nulllable();
            $table->longText('options')->nulllable();
 

            //relación 
            $table->foreign('polls_id')->references('id')->on('polls'); 

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
